import { PostsRepository } from "../repositories/posts.repository";

export class PostsWithOddIdsUseCase {
  static async execute() {
    const repository = new PostsRepository();
    const posts = await repository.getAllPosts();
    return posts.filter((post) => post.id % 2 !== 0);
  }
}
