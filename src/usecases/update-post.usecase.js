import { Post } from "../model/post";
import { PostsRepository } from "../repositories/posts.repository";

export class UpdatePost {
  static async execute(posts = [], postModel) {
    const repository = new PostsRepository();
    const updatePost = await repository.updatePost(postModel.id, postModel);

    const updatePostModel = new Post({
      id: updatePost.id,
      title: updatePost.title,
      content: updatePost.content,
    });

    return posts.map((post) =>
      post.id === updatePostModel.id ? updatePostModel : post
    );
  }
}
