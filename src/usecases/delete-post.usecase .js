import { PostsRepository } from "../repositories/posts.repository";

export class DeletePost {
  static async execute(posts = [], postId) {
    const repository = new PostsRepository();
    await repository.deletePost(postId);
    return posts.filter((post) => post.id !== postId);
  }
}
