import { Post } from "../model/post";
import { PostsRepository } from "../repositories/posts.repository";

export class CreatePost {
  static async execute(body) {
    const repository = new PostsRepository();
    const response = await repository.createPost(body);

    return new Post({
      id: response.id,
      title: response.title,
      content: response.body,
    });
  }
}
