import axios from "axios";

export class PostsRepository {
  async getAllPosts() {
    return await (
      await axios.get("https://jsonplaceholder.typicode.com/posts")
    ).data;
  }

  async deletePost(id) {
    return await (
      await axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`)
    ).data;
  }

  async createPost(body) {
    return await (
      await axios.post("https://jsonplaceholder.typicode.com/posts", body)
    ).data;
  }

  async updatePost(id, body) {
    return await (
      await axios.put(`https://jsonplaceholder.typicode.com/posts/${id}`, body)
    ).data;
  }
}
