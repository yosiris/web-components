import { LitElement, html } from "lit";

export class ListElement extends LitElement {
  static get properties() {
    return {
      post: { type: Object },
    };
  }

  render() {
    return html` <h2 id="title">${this.post?.title}</h2>
      <p id="content">${this.post?.content}</p>`;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("list-element", ListElement);
