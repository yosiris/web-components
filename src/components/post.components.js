import { LitElement, html } from "lit";
import "../ui/list.element";
import { AllPostsUseCase } from "../usecases/all-posts.usecase";
import { UpdatePost } from "../usecases/update-post.usecase";
import { DeletePost } from "../usecases/delete-post.usecase ";
import { CreatePost } from "../usecases/create-post.usecase ";

export class PostsComponent extends LitElement {
  static get properties() {
    return {
      posts: { type: Array },
      addMode: { type: Boolean },
      selectedPost: {
        type: Object,
      },
    };
  }

  constructor() {
    super();
    this.posts = [];
    this.addMode = true;
    this.selectedPost = null;
  }

  async connectedCallback() {
    super.connectedCallback();
    this.posts = await AllPostsUseCase.execute();
  }

  handleSelectPost(post) {
    this.addMode = false;
    this.selectedPost = post;
  }

  async updateSelectedPost(e) {
    e.preventDefault();
    if (!this.selectedPost) {
      console.error("No post selected");
      return;
    }

    const titleInput = document.getElementById("postTitle");
    const contentInput = document.getElementById("postContent");
    const newPost = {
      id: this.selectedPost.id,
      title: titleInput.value,
      content: contentInput.value,
    };

    this.posts = await UpdatePost.execute(this.posts, newPost);
    this.selectedPost = null;
  }

  async addNewPost(e) {
    e.preventDefault();

    const titleInput = document.getElementById("postTitle").value;
    const contentInput = document.getElementById("postContent").value;

    if (!titleInput || !contentInput) {
      alert("Please fill in both title and content fields to add a new post.");
      return;
    }

    const newPost = {
      id: this.posts.length + 1,
      title: titleInput,
      content: contentInput,
    };

    await CreatePost.execute(newPost);
    this.posts = [...this.posts, newPost];
  }

  async delectedPost(e) {
    e.preventDefault();

    const newPostId = {
      id: this.selectedPost.id,
    };

    await DeletePost.execute(this.posts, newPostId);
    this.posts = this.posts.filter((post) => post.id !== this.selectedPost.id);
    this.selectedPost = null;
    this.addMode = true;
  }

  cancelForm(e) {
    e.preventDefault();
    this.addMode = true;
    this.selectedPost = "";
  }

  render() {
    return html`
      <div class="list-content">
        <section class="list-content__list-post">
          <h2 class="list-content__title-post">Post List</h2>
          <button
            class="list-content__add-post list-content--button-color"
            @click="${this.cancelForm}"
          >
            Add
          </button>
          <ul>
            ${this.posts.map(
              (post) =>
                html`<li class="list-content__list">
                  <list-element
                  id="selected-post"
                    .post="${post}"
                    @click="${() => this.handleSelectPost(post)}"></list-element
                  ></list-element>
                </li>`
            )}
          </ul>
        </section>
        <section class="form" id="form">
          <p class="form__form-title">Post Detail</p>
          <form action="submit" method="post" class="form__form-detail">
            <div>
              <label for="title" class="form__label-input">Títle</label>
              <input
                id="postTitle"
                type="text"
                name="title"
                value="${this.selectedPost?.title}"
              />
            </div>
            <div class="form__label-content">
              <label for="content">Content</label>
              <input
                id="postContent"
                type="text"
                name="content"
                class="form__input-content"
                value="${this.selectedPost?.content}"
              />
            </div>
          </form>
          <div id="buttons" class="form__wrapper-butons">
            <button
              id="cancel"
              class="list-content--button-color"
              @click="${this.cancelForm}"
            >
              Cancel
            </button>
            ${this.addMode
              ? html`<button
                  id="addNewPost"
                  class="form__button-add list-content--button-color"
                  @click="${this.addNewPost}"
                >
                  Add
                </button>`
              : html`<button
                    id="delete"
                    class="form__button-delete list-content--button-color"
                    @click="${this.delectedPost}"
                  >
                    Delete
                  </button>
                  <button
                    id="updatePost"
                    class="list-content--button-color"
                    @click="${this.updateSelectedPost}"
                  >
                    Update
                  </button>`}
          </div>
        </section>
      </div>
    `;
  }

  createRenderRoot() {
    return this;
  }
}
customElements.define("list-posts", PostsComponent);
