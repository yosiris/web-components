import { PostsRepository } from "../src/repositories/posts.repository";
import { DeletePost } from "../src/usecases/delete-post.usecase ";

jest.mock("../src/repositories/posts.repository");
describe("Delete post use case", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should delete post post", async () => {
    PostsRepository.mockImplementation(() => {
      return {
        deletePost: () => {},
      };
    });

    const POSTS = [
      {
        userId: 1,
        id: 1,
        title:
          "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        body: "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
      },
      {
        userId: 1,
        id: 2,
        title: "qui est esse",
        body: "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla",
      },
    ];

    const postId = 1;
    const deletePost = await DeletePost.execute(POSTS, postId);

    expect(deletePost.length).toBe(POSTS.length - 1);
    expect(deletePost[0].id).toBe(2);
  });
});
