import { Post } from "../src/model/post";
import { PostsRepository } from "../src/repositories/posts.repository";
import { CreatePost } from "../src/usecases/create-post.usecase ";

jest.mock("../src/repositories/posts.repository");

describe("Create new post", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should create a new post", async () => {
    const request = {
      userId: 1,
      title: "qui est esse",
      body: "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae u eiciendis\nqui aperiam non debitis possimus qui neque nisi nulla",
    };
    PostsRepository.mockImplementation(() => {
      return {
        createPost: (request) => {
          return {
            userId: request.userId,
            title: request.title,
            body: request.body,
            id: 101,
          };
        },
      };
    });

    const post = await CreatePost.execute(request);

    const expectedPost = new Post({
      id: 101,
      title: request.title,
      content: request.body,
    });
    expect(post).toEqual(expectedPost);
  });
});
