import { Post } from "../src/model/post";
import { PostsRepository } from "../src/repositories/posts.repository";
import { UpdatePost } from "../src/usecases/update-post.usecase";

jest.mock("../src/repositories/posts.repository");

describe("Update existing post", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should update an existing post with the given properties", async () => {
    const request = [
      {
        userId: 1,
        title: "qui est esse",
        body: "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae u eiciendis\nqui aperiam non debitis possimus qui neque nisi nulla",
      },
    ];

    const expectedPost = new Post({
      id: 1,
      userId: request.userId,
      title: request.title,
      content: request.body,
    });
    PostsRepository.mockImplementation(() => {
      return {
        updatePost: () => {
          return {
            id: expectedPost.userId,
            title: expectedPost.title,
            body: expectedPost.body,
            userId: 1,
          };
        },
      };
    });
    const post = await UpdatePost.execute(request, expectedPost);

    expect(post.length).toBe(1);
    expect(post[0].title).toEqual(expectedPost.title);
    expect(post[0].content).toBe(expectedPost.content);
  });
});
