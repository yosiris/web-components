/// <reference types="Cypress" />

describe("template spec", () => {
  it("should navegation", () => {
    cy.visit("http://localhost:8080/");
  });
});

describe("list post form", () => {
  it("should cancel post", () => {
    cy.visit("http://localhost:8080/");
    cy.get("#cancel").click();
  });
});

describe("add new post", () => {
  it("should create new post", () => {
    cy.visit("http://localhost:8080/");
    cy.get("#addNewPost").click();
  });
});

describe("template spec", () => {
  it("should detail posts", () => {
    cy.visit("http://localhost:8080/");
    cy.get(":nth-child(1) > list-element").click();
  });
});

describe("template spec", () => {
  it("should update and delete posts", () => {
    cy.visit("http://localhost:8080/");
    cy.get(":nth-child(1) > list-element").click();
  });
});

describe("template spec", () => {
  it("should add title and content text to input", () => {
    cy.visit("http://localhost:8080/");
    cy.get("#postTitle").type("text title");
    cy.get("#postContent").type("text content");
  });
});

describe("template spec", () => {
  it("should add title and content text to input", () => {
    cy.visit("http://localhost:8080/");

    cy.get("#buttons");
  });
});
